#include <Doze.h>

#define OUTPUT_PIN 0 // piezo wired to pin 0

Doze dozer(DURATION_8S);

void setup()
{  
  pinMode(0, OUTPUT);
  pinMode(1, OUTPUT);
  pinMode(A1, OUTPUT);
  pinMode(A2, OUTPUT);
  pinMode(A3, OUTPUT);

  beep(100);
  beep(100);
  beep(100);
  delay(1000);
}

void loop()
{
  randomSeed(analogRead(A1));
  beep(random(500,1500));

  long cycles = random(2,5); // ~2-5 mins
  cycles *= 8;

  do
  {
     dozer.pause();
  } while(--cycles > 0);
}

void beep(long ms)
{
  digitalWrite(OUTPUT_PIN, HIGH);
  delay(ms);
  digitalWrite(OUTPUT_PIN, LOW);
  delay(ms);
}

